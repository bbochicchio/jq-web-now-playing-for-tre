// JavaScript Document
// TRE XML Song Data Loader by Brian Bochicchio 
// This is provided as and is not a product of or Supported by Broadcast Electronics Inc.
// This JS fetchs a TRE XML and grabs the first EventData which is always current event
// You can Navigate the Nodes to get the data you want to display in the HTML page included with the project
// To minimize cross-domain issues nowplaying.xml is assumed to be the same folder as the html page

function loadXMLDoc(url)
{
var xmlhttp;
var txt,x,xx;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    x=xmlhttp.responseXML.documentElement.getElementsByTagName("EventData");
      //need to add try..catch 
	  xx=x[0].getElementsByTagName("Event_Type")
	  txt=xx[0].firstChild.nodeValue;
	  //console.log(txt);
      if ( txt != "MUSIC"){
		  document.getElementById('currentTitle').innerHTML="Who stole the music?";
		  document.getElementById('currentArtist').innerHTML="";
	  } else {
		  
		  xx=x[0].getElementsByTagName("Title");

      txt=xx[0].firstChild.nodeValue;
	  if (txt !=""){
		  document.getElementById('currentTitle').innerHTML=txt;
	  } else {
		  document.getElementById('currentTitle').innerHTML="Guess the Song";		  
	  }
		  
      xx=x[0].getElementsByTagName("Artist");
      txt=xx[0].firstChild.nodeValue;
	  if (txt !=""){
		  document.getElementById('currentArtist').innerHTML="by: " + txt;
	  }else {
		  document.getElementById('currentArtist').innerHTML="Guess the Artist";
	  }

		  
	  }
	  
	  	
    }
  }
xmlhttp.open("GET",url,true);
xmlhttp.send();
}
   
