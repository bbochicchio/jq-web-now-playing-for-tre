// JavaScript Document
// TRE XML Song Data Loader by Brian Bochicchio 
// This is provided as and is not a product of or supported by Broadcast Electronics Inc.
// This timer is used for (re)load the XML nowplaying, accuracy is based on user delay to stream.
// Typically an 8-10 second cycle will allow for a aliitel drift and not wreck the server with usless requests
// To minimize cross-domain issues nowplaying.xml is assumed to be the same folder as the html page

var timer_is_on = 0;
var c = 0;

function startTimer()
{
	console.log("Starting timer");
	
if (!timer_is_on)
  {
  timer_is_on=1;
  timerCount();
  }
}

function timerCount()
{
c += 1;
// Prints the Timer Value to the Chrome Console.  Remark out for Prodcution
//console.log(c);

// Checks to see if 10 seconds has passed. If so, go get new song data.
if ( c % 10 == 0) {
	// Writes to the console gonfriming it tried to fetch new meta data. Remark out for Prodcution
	console.log("Trying to refresh now playing");
	loadXMLDoc('nowplaying.xml')
	//loadXMLDoc('../nowplaying.xml');
}


t=setTimeout("timerCount()",1000);
}

function stopTimer()
{
clearTimeout(t);
timer_is_on=0;
}
